FROM nginx:alpine

# Install git
RUN apk add --no-cache git

# Set the working directory
WORKDIR /usr/share/nginx/html

# clone the repository for 5etools (src only)
RUN rm -rf * && git clone --depth 1 https://github.com/5etools-mirror-3/5etools-2014-src.git .

EXPOSE 80

VOLUME ["/usr/share/nginx/html/homebrew"]

CMD ["nginx", "-g", "daemon off;"]