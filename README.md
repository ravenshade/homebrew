# Ravenshade Homebrew

Automatically deployed to 5etools as homebrew.

Update ravenshade.json (compiled and committed) to apply homebrew changes.

## Building sources
Sources are spread across multiple `.json`/`.js` files for ease. They can be built with a script.

```sh
npm run build
```

### Local Development

You can run 5etools in a container that mounts this homebrew directory for local development.

```sh
# will start 5etools on localhost:8080 with /homebrew mounted 
npm run dev
```

## 5etools homebrew

The top level document must match the [homebrew schema](https://github.com/TheGiddyLimit/homebrew/blob/master/_schema/homebrew.json).

Other types of objects can be found in the [schemas dir](https://github.com/TheGiddyLimit/homebrew/tree/master/_schema).

If you need to validate against the schema, use [my fork of the homebrew tools](https://gitlab.com/ravenshade/5etools-homebrew-validator).