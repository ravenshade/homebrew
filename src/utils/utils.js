import { readdir, readFile } from "fs/promises";
import { extname, join } from 'path';

export async function readJsonFilesInDir(directoryPath) {
  try {
    const files = await readdir(directoryPath);
    const jsonFiles = files.filter(file => extname(file) === '.json');
    const objects = [];

    for (let file of jsonFiles) {
      const filePath = join(directoryPath, file);
      const promise = readFile(filePath, 'utf8').then(data => JSON.parse(data));
      objects.push(promise);
    }

    return await Promise.all(objects);
  } catch (error) {
    console.error('Error reading JSON files:', error);
    throw error; // Rethrow or handle as needed
  }
}

/**
 * Only merges at top level arrays
 */
export function mergeModule(source, module, moduleTag="module") {
  for (const key in module) {
    if (key in source) {
      if (!Array.isArray(source[key]) || !Array.isArray(module[key])) {
        console.warn(`[build] Source['${key}'] cannot merge ${moduleTag}['${key}'] because it is not an array.`)
        continue;
      }
      source[key] = source[key].concat(module[key]);
      console.log(`[${moduleTag}] merged ${key}`, module[key].filter(item => item.name != null).map(item => item.name));
    } else {
      source[key] = module[key];
      console.log(`[${moduleTag}] added ${key}`, module[key].filter(item => item.name != null).map(item => item.name));
    }
  }
  return source;
}

export function printSource(name, source) {
  console.log(`Source ${name}`);
  for (const key in source) {
    console.log(`  ${key}`);
    if (Array.isArray(source[key])) {
      for (const item of source[key]) {
        if (!('name' in item))
          continue;
        console.log(`     - ${item.name}`);
      }
    }
  }
}