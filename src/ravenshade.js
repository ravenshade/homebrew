import pianta from "./pianta.json" with { type: "json" };
import gryphonwarden from "./gryphonwarden.json" with { type: "json" };
import ravenrook from "./ravenrook.json" with { type: "json" };
import riven from "./riven.json" with { type: "json" };
import explorers from "./explorers.json" with { type: "json" };


import { mergeModule, printSource } from "./utils/utils.js";

const source = {
	"_meta": {
		"sources": [
			{
				"json": "Ravenshade",
				"full": "Ravenshade",
				"abbreviation": "RS",
				"authors": [
					"Kenny"
				],
				"convertedBy": [
					"Kenny"
				],
				"url": "https://gitlab.com/ravenshade",
				"version": "01JAN2022"
			}
		],
		"dateAdded": 1642018371,
		"dateLastModified": 1642018371
	}
}

console.log("Building Source: ravenshade...\n")
// can be done programmatically in future if needed, for now lazy
mergeModule(source, pianta, "pianta");
mergeModule(source, gryphonwarden, "gryphon");
mergeModule(source, ravenrook, "ravenrook");
mergeModule(source, riven, "riven");
mergeModule(source, explorers, "explorers");

console.log('---------------------------------');
printSource("ravenshade", source);

export default source;